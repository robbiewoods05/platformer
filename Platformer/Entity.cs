using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Platformer
{
    public class Entity
    {
        public static int Width { get; protected set; }
        public static int Height { get; protected set; }

        public bool IsSolid { get; protected set; }

        public Vector2 Position { get; protected set; }

        public Texture2D Sprite { get; protected set; }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, Color.White);
        }
     }
}
