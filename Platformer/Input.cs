﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer
{
    public class InputHandler : GameComponent
    {
        #region Fields

        static KeyboardState lastKeyboardState;
        static KeyboardState currentKeyboardState;

        private static GamePadState[] gamePadStates;
        private static GamePadState[] lastGamePadStates;

        #endregion

        #region Properties 

        public static KeyboardState LastKeyboardState => lastKeyboardState;

        public static KeyboardState CurrentKeyboardState => currentKeyboardState;

        public static GamePadState[] GamePadStates => gamePadStates;

        public static GamePadState[] LastGamePadStates => lastGamePadStates;

        #endregion

        #region Constructor

        public InputHandler(Game game) : base(game)
        {
            currentKeyboardState = Keyboard.GetState();

            //foreach (PlayerIndex i in Enum.GetValues(typeof (PlayerIndex)))
            //gamePadStates[(int) i] = GamePad.GetState(i);
        }

        #endregion

        #region XNA Methods

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            lastKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            //lastGamePadStates = (GamePadState[]) gamePadStates.Clone();
            //  foreach (PlayerIndex i in Enum.GetValues(typeof(PlayerIndex)))
            //  gamePadStates[(int) i] = GamePad.GetState(i);

            base.Update(gameTime);
        }

        #endregion

        #region General Methods

        public static void Flush()
        {
            lastKeyboardState = currentKeyboardState;
        }

        #endregion

        #region Keyboard Methods

        public static bool KeyReleased(Keys k) => currentKeyboardState.IsKeyUp(k) && lastKeyboardState.IsKeyDown(k);

        public static bool KeyPressed(Keys k) => currentKeyboardState.IsKeyDown(k) && lastKeyboardState.IsKeyUp(k);

        public static bool KeyDown(Keys k) => currentKeyboardState.IsKeyDown(k);

        #endregion

        #region Gamepad Methods

        public static bool ButtonReleased(Buttons b, PlayerIndex i)
            => gamePadStates[(int)i].IsButtonDown(b) && lastGamePadStates[(int)i].IsButtonUp(b);

        public static bool ButtonPressed(Buttons b, PlayerIndex i)
            => gamePadStates[(int)i].IsButtonUp(b) && lastGamePadStates[(int)i].IsButtonDown(b);

        public static bool ButtonDown(Buttons b, PlayerIndex i) => gamePadStates[(int)i].IsButtonDown(b);

        #endregion
    }
}
