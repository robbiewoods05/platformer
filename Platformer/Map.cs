using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Platformer
{
    class Map
    {
        private int[,] _tileMap;

        // TODO: If I ever make it this far, make the maps bigger and introduce culling so that only on screem titles are drawn
        private int _mapWidth =  25, _mapHeight = 15;

        private Map()
        {
            _tileMap = new int[_mapWidth, _mapHeight];
        }

        public static Map LoadMap(string mapPath)
        {
            string[] mapString;
            string mapFileName = Path.GetFileNameWithoutExtension(mapPath);

            Map m = new Map();

            try
            {
                mapString = File.ReadAllLines(mapPath);

                if (mapString.Length == 0)
                    throw new InvalidDataException();
            }
            catch
            {
                MessageBox.Show($"Map \"{mapFileName}\" unable to be loaded", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return null;
            }

            for (int i =  0; i < m._mapHeight; i++)
            {
                string[] currentLine = mapString[i].Split(',');

                for (int j = 0; j < m._mapWidth; j++)
                {
                    int id;

                    if(!int.TryParse(currentLine[j], out id))
                    {
                        MessageBox.Show($"Bad data found in map \"{mapFileName}\"", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        return null;
                    }

                    m._tileMap[j, i] = id;
                }
            }

            return m;
        }

        public static void Draw(SpriteBatch spriteBatch, Map m)
        {
            Tile current; 

            for (int x = 0; x < m._mapWidth; x++)
            {
                for (int y = 0; y < m._mapHeight; y++)
                {
                    current = Tile.Tiles.FirstOrDefault(t => t.ID == m._tileMap[x, y]);

                    if (current != null)
                        spriteBatch.Draw(Tile.TileSet, new Rectangle(x*32, y*32, 32, 32), current.QuadPosition, Color.White);
                }
            }
        }

    }
}
