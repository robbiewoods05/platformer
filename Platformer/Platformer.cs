using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Platformer
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Platformer : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Map map1;
        Tile rock, grass, lava;
        Player player;
        State<GameState> gameState = new State<GameState>();
        float dt, playerHVelocity, playerYVelocity;

        public Platformer()
        {
            graphics = new GraphicsDeviceManager(this);
     
            Components.Add(new InputHandler(this));
            Content.RootDirectory = "Content";
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here  

            map1 = Map.LoadMap(@"Maps/test.txt");
            rock = new Tile(1, true, new Rectangle(17*32, 16*32, 32, 32));
            grass = new Tile(2, true, new Rectangle(0, 13*32, 32, 32));
            lava = new Tile(3, false, new Rectangle(50 * 32, 13 * 32, 32, 32));

            gameState.Push(GameState.MainMenu);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            Tile.TileSet = Content.Load<Texture2D>("tileset.png");
            player = new Player(Content.Load<Texture2D>("player_frame0.gif"), new Vector2(0, 480 - 96));

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (gameState.CurrentState == GameState.MainMenu)
            {
                if (InputHandler.KeyDown(Keys.Enter))
                    gameState.Push(GameState.Play);
            }
            if (gameState.CurrentState == GameState.Play)
            {
                playerHVelocity = InputHandler.KeyDown(Keys.Left) ? -1 : InputHandler.KeyDown(Keys.Right) ? 1 : 0;
                playerYVelocity = InputHandler.KeyDown(Keys.Up) ? -1 : InputHandler.KeyDown(Keys.Down) ? 1 : 0;

                if (InputHandler.KeyReleased(Keys.Space))
                    playerYVelocity = -30f;

                player.Update(dt, new Vector2(playerHVelocity, playerYVelocity));
            }

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(88, 7, 92));

            spriteBatch.Begin();

            if (gameState.CurrentState == GameState.MainMenu)
            {

            }
            else if (gameState.CurrentState == GameState.Play)
            {
                Map.Draw(spriteBatch, map1);
                player.Draw(spriteBatch);
            }
            else if (gameState.CurrentState == GameState.Options)
            {

            }

            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
