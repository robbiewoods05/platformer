﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer
{
    public class Player : Entity
    {
        public float Speed { get; } = 150;
        public int Score { get; set; } = 0;
        public int Health { get; set; } = 100;
        public new Vector2 Position => _position;

        private float _gravity = 60f, _yVelocity, _xVelocity; 

        private Vector2  _position;

        public bool FacingRight = true;

        public Player(Texture2D sprite, Vector2 startPos)
        {
            Sprite = sprite;
            _position = startPos;

            Width = Sprite.Width;
            Height = Sprite.Height;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (FacingRight)
                spriteBatch.Draw(Sprite, _position, Color.White);
            else
                spriteBatch.Draw(Sprite, _position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.FlipHorizontally, 0f);
        }

        public void Update(float dt, Vector2 vel)
        {
            if (vel.X < 0)
                FacingRight = false;
            else if(vel.X > 0)
                FacingRight = true;

            _yVelocity = vel.Y;
            _xVelocity = vel.X;

            _xVelocity *= (Speed * dt);
            _yVelocity *= (Speed * dt);

            _yVelocity += _gravity * dt;

            if ((int)_position.Y >= 480 - 96 && _yVelocity > 0)
                _yVelocity = 0;

            _position.X += _xVelocity;
            _position.Y += _yVelocity;
        }
    }
}
