﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformer
{
    public class State<T>
    {
        public T CurrentState => _states.Peek();

        public void Pop()
        {
            _states.Pop();
        }

        public void Push(T state)
        {
            _states.Push(state);
        }

        private Stack<T> _states = new Stack<T>();
    }
}
