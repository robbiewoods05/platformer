using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Platformer
{
    public class Tile : Entity
    {
        public static Texture2D TileSet;
        private Rectangle _quadPosition;
        public Rectangle QuadPosition => _quadPosition;

        public int ID => _id;
        private int _id;
        private static List<Tile> _tiles = new List<Tile>();
        public static List<Tile> Tiles => _tiles;

        public Tile(int id, bool solid, Rectangle quadPos)
        {
            _id = id;
            IsSolid = solid;

            Width = 32;
            Height = 32;

            _quadPosition = quadPos;

            _tiles.Add(this);
        }

    }
}
